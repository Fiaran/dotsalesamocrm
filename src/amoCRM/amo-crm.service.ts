import { Injectable } from '@nestjs/common';
import axios from 'axios';
import * as fs from 'fs/promises';
import {Cron} from "@nestjs/schedule";
import * as dotenv from 'dotenv';
dotenv.config();

@Injectable()
export class AmoCrmService {
  private initialized = false;
  constructor() {
    this.init().then(() => {
      this.initialized = true;
    })
  }

  public async init(){
    try {
      const endpoint = 'https://mnurmatov.amocrm.ru/oauth2/access_token';
      const tokens =  await axios.post(
          endpoint,
          {
            client_id: process.env.CLIENT_ID,
            client_secret: process.env.CLEINT_SECRET,
            grant_type: process.env.GRANT_TYPE,
            code: process.env.CODE,
            redirect_uri: process.env.REDIRECT_URI
          }).then((request) =>{
        return request.data;
      }).catch((e) =>{
        return e;
      });
      console.log("tokens = ", tokens);
      await fs.writeFile('./src/amoCRM/tokens/tokens.json', JSON.stringify(tokens, null, 2));
      return 'success';
    }catch (e){
      return e;
    }
  }

  @Cron('0 0 */3 * * *')
  async refreshTokenIfNeeded() {
     try {
       console.log('TOKEN WAS UPDATED BY CRON');
       const fileContents = await fs.readFile('./src/modules/amo-crm/tokens/tokens.json', 'utf-8');
       const tokens = JSON.parse(fileContents);
       const refreshToken = tokens.refresh_token;

         let json = {
           client_id: process.env.CLIENT_ID,
           client_secret: process.env.CLIENT_SECRET,
           grant_type: process.env.GRANT_REFRESH,
           refresh_token: refreshToken,
           redirect_uri: process.env.REDIRECT_URI
         };
         let res = await axios.post(
             `https://kgtradeweb.amocrm.ru/oauth2/access_token`,
             json,
         );

         const newAccessToken = res.data.access_token;
         const newRefreshToken = res.data.refresh_token;
         await this.updateTokensFile(newAccessToken, newRefreshToken);
     }catch (e){
       console.log('refreshTokenIfNeeded = ', e);
       return {
         message: "failed"
       }
     }

  }

  async updateTokensFile(newAccessToken, newRefreshToken) {
    try {
      const fileContents = await fs.readFile('./src/amoCRM/tokens/tokens.json', 'utf-8');
      const tokens = JSON.parse(fileContents);
      tokens.access_token = newAccessToken;
      tokens.refresh_token = newRefreshToken;
      const updatedJson = JSON.stringify(tokens, null, 2); // Adding null and 2 for pretty formatting
      await fs.writeFile('./src/amoCRM/tokens/tokens.json', updatedJson, 'utf-8');
      console.log('Tokens updated successfully.');
    } catch (error) {
      console.error('Error updating tokens in file:', error.message);
    }
  }

}






