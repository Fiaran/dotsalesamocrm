import { Module } from '@nestjs/common';
import { AmoCrmService } from './amo-crm.service';

@Module({
  controllers: [],
  providers: [AmoCrmService],
})
export class AmoCrmModule {}
