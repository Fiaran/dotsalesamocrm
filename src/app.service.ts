import { Injectable } from '@nestjs/common';
import axios from "axios";
import * as fs from 'fs/promises';

@Injectable()
export class AppService {
  public async getLeads(query?: string) {
    const fileContents = await fs.readFile('./src/amoCRM/tokens/tokens.json', 'utf-8');
    const tokens = JSON.parse(fileContents);
    const accessToken = tokens.access_token;

    if(query){
        if (query.length > 3) {
            const res =  await axios.get('https://mnurmatov.amocrm.ru/api/v4/leads?order[updated_at]=asc', {
                    headers: {
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${accessToken}`,
                    }, params: {
                        with: 'contacts',
                        query
                    }
                }
            );
            console.log(res.data);
            return res.data;
        }
    }
    else{
      const res =  await axios.get('https://mnurmatov.amocrm.ru/api/v4/leads?order[updated_at]=asc', {
            headers: {
              'Content-Type': 'application/json',
              Authorization: `Bearer ${accessToken}`,
            }, params: {
              with: 'contacts',
            }
          }
      );
      console.log(res.data);
      return res.data;
    }
  }
}
